package net.colt.novitas.pricing;

public enum Pricebook {
	FLEX,
	FIXED_ENT,
	FIXED_WHL
}
