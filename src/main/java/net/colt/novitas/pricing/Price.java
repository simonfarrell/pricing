package net.colt.novitas.pricing;

public class Price {
	
	private float rental;
	private String currency;
	private int bandwidth;
	private Commitment commitment;
	private String country;
	private String metro;
	private int producttype;
	private String sitetype;
	
	public Price() {
	}
	
	public Price(PortData pd) {
		this.bandwidth = pd.getBandwidth();
		this.country = pd.getCountry();
		this.metro = pd.getMetro();
		this.producttype = pd.getProducttype();
		this.sitetype = pd.getSitetype();
	}

	public Price(ConnectionData cd) {
		
	}
	
	
	public int getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(int bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public float getRental() {
		return rental;
	}

	public void setRental(float rental) {
		this.rental = rental;
	}
	
	public void setRentalDouble(double r) {
		this.rental = (float) r;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setSitetype(String s) {
		this.sitetype = s;
	}
	
	public String getSitetype() {
		return sitetype;
	}

	public int getProducttype() {
		return producttype;
	}

	public void setProducttype(int porttype) {
		this.producttype = porttype;
	}

	public String getMetro() {
		return metro;
	}

	public void setMetro(String metro) {
		this.metro = metro;
	}

	public Commitment getCommitment() {
		return commitment;
	}

	public void setCommitment(Commitment commitment) {
		this.commitment = commitment;
	}
	
}
