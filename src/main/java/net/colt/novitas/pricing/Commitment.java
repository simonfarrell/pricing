package net.colt.novitas.pricing;

public enum Commitment {
	NONE,
	MONTHS_12,
	MONTHS_24,
	MONTHS_36
}
