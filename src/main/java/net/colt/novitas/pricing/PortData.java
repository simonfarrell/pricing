package net.colt.novitas.pricing;

public class PortData {
	
	public static final int AT = 0;
	public static final int UK = 1;
	
	public static final int ETHERNET_PORT=0;
	public static final int CLOUD_HOSTED_PORT=1;
	public static final int AWS_DEDICATED_PORT=2;
	public static final int AZURE_CLOUD_PORT=3;
	public static final int GCP_CLOUD_PORT=3;
	
	private int bandwidth;
	private String country;
	private String metro;
	private int producttype;
	private String sitetype;
	private int availablecapacity;
	private LocationData zone;
	private Pricebook pricebook;

	public PortData() {
		zone = LocationData.NOZONE;
	}

	public int getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(int bandwidth) {
		this.bandwidth = bandwidth;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setSitetype(String s) {
		this.sitetype = s;
	}
	
	public String getSitetype() {
		return sitetype;
	}

	public int getProducttype() {
		return producttype;
	}

	public void setProducttype(int porttype) {
		this.producttype = porttype;
	}

	public String getMetro() {
		return metro;
	}

	public void setMetro(String metro) {
		this.metro = metro;
	}

	public LocationData getZone() {
		if(zone == null) return LocationData.NOZONE;
		return zone;
	}

	public void setZone(LocationData zone) {
		this.zone = zone;
	}

	public void setPricebook(Pricebook pb) {
		this.pricebook = pb;		
	}
	
	public Pricebook getPricebook() {
		return this.pricebook;
	}

	public int getAvailablecapacity() {
		return availablecapacity;
	}

	public void setAvailablecapacity(int availablecapacity) {
		this.availablecapacity = availablecapacity;
	}
}