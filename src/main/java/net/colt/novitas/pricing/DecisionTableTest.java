package net.colt.novitas.pricing;

import java.util.HashMap;
import java.util.Map;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

/**
 * This is a sample class to launch a rule.
 */
public class DecisionTableTest {

    public static final void main(String[] args) {
    	new DecisionTableTest();
    }
    
    public DecisionTableTest() {
        try {
            // load up the knowledge base
	        KieServices ks = KieServices.Factory.get();
    	    KieContainer kContainer = ks.getKieClasspathContainer();
        	KieSession kSession = kContainer.newKieSession("ksession-dtables");

        	// Output map
            Map<Integer, Price> matched_prices = new HashMap<Integer, Price>();
            
            // One-off port test
//            PortData message = makePortData("BE", "Brussels","KDC", Pricebook.FLEX);
//            System.out.println("Matching prices for BE/Brussels/KDC with Flex pricebook");
            
            // Connection test
            ConnectionData message1 = makeInternational();
            showConnection(message1);
//            ConnectionData message2 = makeMetro();
//            showConnection(message2);
            kSession.setGlobal("matched_prices", matched_prices);
            kSession.insert(message1);
//            kSession.insert(message2);
            kSession.fireAllRules();
            showPrices(matched_prices);
            showConnection(message1);
//            showConnection(message2);
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }
    
    private ConnectionData makeMetro() {
        PortData a_end = makePortData("BE", "Brussels", "KDC", Pricebook.FLEX);
        a_end.setAvailablecapacity(100);
        a_end.setProducttype(PortData.ETHERNET_PORT);
        PortData b_end = makePortData("BE", "Brussels", "CLOUD", Pricebook.FLEX);
        b_end.setAvailablecapacity(100);
        b_end.setProducttype(PortData.AWS_DEDICATED_PORT);
        ConnectionData cd = new ConnectionData(a_end, b_end, Pricebook.FLEX);
    	return cd;
    	
    }
    
    private ConnectionData makeInternational() {
        PortData a_end = makePortData("BE", "Brussels", "KDC", Pricebook.FLEX);
        a_end.setAvailablecapacity(100);
//        a_end.setZone(LocationData.EU1);
        PortData b_end = makePortData("NL", "Amsterdam", "CLOUD", Pricebook.FLEX);
        b_end.setAvailablecapacity(100);
//        b_end.setZone(LocationData.EU1);
        b_end.setProducttype(PortData.AWS_DEDICATED_PORT);
        ConnectionData cd = new ConnectionData(a_end, b_end, Pricebook.FLEX);
//      cd.setInternationalzone(1);

    	return cd;
    }
    
    private void showConnection(ConnectionData c) {
    	System.out.println("Connection:");
    	System.out.println("Reach: " + c.getReach());
    	System.out.println("A end Product Type: "+ c.getA_end().getProducttype());
    	System.out.println("A end Site Type: "+ c.getA_end().getSitetype());
    	System.out.println("A Zone: " + c.getA_end().getZone());
    	System.out.println("B end Product Type: "+ c.getB_end().getProducttype());
    	System.out.println("B end Site Type: "+ c.getB_end().getSitetype());
    	System.out.println("B Zone: " + c.getB_end().getZone());
    	System.out.println("Intl Zone: " + c.getInternationalzone());
    }
    
    
    private void showPrices(Map<Integer, Price> matched_prices) {
    	System.out.println("------ Matched Prices ---------");
        for( Price p : matched_prices.values()) {
        	System.out.println("BW: " + Integer.toString(p.getBandwidth()) + ", Price: "+Float.toString(p.getRental())+" "+ p.getCurrency());
        }
    	System.out.println("-------------------------------");
    }


    private PortData makePortData(String country, String metro, String sitetype, Pricebook pb) {
    	PortData message = new PortData();
        message.setCountry(country);
        message.setMetro(metro);
        message.setSitetype(sitetype);
        message.setPricebook(pb);
        message.setProducttype(PortData.ETHERNET_PORT);
        return message;
    }

}
