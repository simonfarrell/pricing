package net.colt.novitas.pricing;

public enum LocationData {
	NOZONE,
	EU1,
	EU2,
	EU3,
	EE1,
	ASIA1,
	ASIA2,
	ASIA3,
	USA1,
	AU1
}
