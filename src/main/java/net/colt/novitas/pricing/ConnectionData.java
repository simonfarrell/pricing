package net.colt.novitas.pricing;

public class ConnectionData {
	public static final int ETHERNET_CONNECTION=0;
	public static final int CLOUD_CONNECTION=1;
	public static final int AZURE_CONNECTION=2;

	public static final int INTERNATIONAL=0;
	public static final int NATIONAL=1;
	public static final int METRO=2;
	
	private int bandwidth;
	private int reach;
	private int internationalzone;
	private PortData a_end;
	private PortData b_end;
	private PortData b_end2;
	private Pricebook pricebook;

	

	public ConnectionData(PortData a, PortData b, Pricebook p) {
		this.a_end = a;
		this.b_end = b;
		this.setPricebook(p);
		this.calculateReach();
		this.internationalzone = 0; 
	}

	private void calculateReach() {
		String a_country = a_end.getCountry();
		String b_country = b_end.getCountry();
		String a_metro = a_end.getMetro();
		String b_metro = b_end.getMetro();
		
		if(a_country.equalsIgnoreCase(b_country)) {
			if(a_metro.equalsIgnoreCase(b_metro)) {
				this.reach = METRO;
			} else {
				this.reach = NATIONAL;
			}
		} else {
			this.reach = INTERNATIONAL;
		}
		
	}

	public int getBandwidth() {
		return bandwidth;
	}

	public void setBandwidth(int bandwidth) {
		this.bandwidth = bandwidth;
	}

	public PortData getA_end() {
		return a_end;
	}

	public void setA_end(PortData a_end) {
		this.a_end = a_end;
	}

	public PortData getB_end() {
		return b_end;
	}

	public void setB_end(PortData b_end) {
		this.b_end = b_end;
	}

	public PortData getB_end2() {
		return b_end2;
	}

	public void setB_end2(PortData b_end2) {
		this.b_end2 = b_end2;
	}

	public int getReach() {
		return reach;
	}

	public Pricebook getPricebook() {
		return pricebook;
	}

	public void setPricebook(Pricebook pricebook) {
		this.pricebook = pricebook;
	}

	public int getInternationalzone() {
		return internationalzone;
	}

	public void setInternationalzone(int internationalzone) {
		this.internationalzone = internationalzone;
	}



}
